import numpy as np

def scorer(y_true, y_pred, num_users=53979):
    '''
        `y_true` and `y_pred` are dictionaries of type {user: items_list}

        `num_users` is the number of users in training set.
        The scorer expects predictions for exactly `ceil(num_users*0.05)` users

        For private and public leaderboard evaluation:
            - for the track one scorer `num_users` is equal to 1079572
            - for the track two `num_users=100000`
    '''

    num_users_5p = np.ceil(0.05 * num_users)

    # Check everything is correct
    assert type(y_true) == type(y_pred) == dict, 'Need `y_pred` and `y_true` to be dictionaries.'
    assert len(y_pred) == num_users_5p, 'Found predictions for %d users, instead of %d.' % (len(y_pred), num_users_5p)
    assert np.all([len(x) == 5 for x in y_pred.values()]), 'Please, submit exactly 5 items per user.'

    # Compute score
    score = 0
    for user, items_pred in y_pred.items():
        items_true = y_true.get(user, [])
        score += len(set(items_true) & set(items_pred)) > 0

    return score / float(len(y_pred)) * 10000.0
