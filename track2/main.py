import pandas as pd
import numpy as np

from scipy.sparse import csr_matrix
from sklearn.decomposition import TruncatedSVD

# Parameters of the task
input_path = "./train.csv.zip"
output_path = "./submission.csv"
n_predict_items = 5
n_predict_users = 5000
n_compute_users = 100000
forbidden_begin = 41
forbidden_end = 62
zero_threshold = 10e-6
prediction_columns = ["user_id", "id3_1", "id3_2", "id3_3", "id3_4", "id3_5"]

# Parameters of the truncated SVD
n_components = 400
n_iter = 42
random_state = 42


def cat_values_extractor(values):
    num_to_cat = list(set(values))
    n_cats = len(num_to_cat)
    cat_to_num = {cat: i for i, cat in enumerate(num_to_cat)}
    return n_cats, num_to_cat, cat_to_num


def build_views_matrix(train, n_compute_users, n_items,
                       user_id_to_num, id3_to_num):
    views = np.zeros((n_compute_users, n_items), dtype="uint8")
    for x, y in zip(train.user_id.values, train.id3.values):
        views[user_id_to_num[x], id3_to_num[y]] += 1

    return csr_matrix(views, dtype="float32")


def extract_forbidden_items(train, forbidden_begin, forbidden_end):
    from collections import defaultdict

    forbidden_items = defaultdict(set)
    values = zip(train.user_id.values, train.date.values, train.id3.values)
    for user_id, date, item_id in values:
        if forbidden_begin <= date < forbidden_end:
            forbidden_items[user_id].add(item_id)
    return forbidden_items


def build_train_data():
    train =\
        pd.read_csv(input_path, dtype="int32", compression="zip", engine="c")

    n_items, num_to_id3, id3_to_num =\
        cat_values_extractor(train.id3.values)
    _, num_to_user_id, user_id_to_num =\
        cat_values_extractor(train.user_id.values)

    views = build_views_matrix(train, n_compute_users, n_items,
                               user_id_to_num, id3_to_num)
    forbidden_items = extract_forbidden_items(train,
                                              forbidden_begin, forbidden_end)

    return num_to_id3, id3_to_num, num_to_user_id, user_id_to_num,\
           views, forbidden_items


def predict(forbidden_items, U, Vt, num_to_user_id, num_to_id3):
    shape = (n_predict_users, n_predict_items + 1)
    predictions = np.ndarray(shape, dtype="int32")
    Vt_rnorms = np.sqrt((Vt * Vt).sum(axis=0))
    Vt_rnorms[Vt_rnorms < zero_threshold] = 1.0
    Vt_rnorms = 1.0 / Vt_rnorms

    for i in range(n_predict_users):
        user_id = num_to_user_id[i]
        user_vector = U[i].reshape(-1, 1)
        user_rnorm = 1.0 / np.sqrt((U[i] * U[i]).sum())
        cosines = (user_vector * Vt).sum(axis=0) * user_rnorm * Vt_rnorms

        relevance_per_item = [(num_to_id3[j], c) for j, c in enumerate(cosines)]
        relevance_per_item.sort(key=lambda t: -t[1])

        n_chosen = 0
        for item_id, _ in relevance_per_item:
            if item_id not in forbidden_items[user_id]:
                if n_chosen >= 5:
                    break
                n_chosen += 1
                predictions[i, n_chosen] = item_id
        predictions[i, 0] = user_id
    return predictions


if __name__ == "__main__":
    num_to_id3, id3_to_num, num_to_user_id, user_id_to_num, views,\
    forbidden_items = build_train_data()

    svd = TruncatedSVD(n_components, n_iter=n_iter, random_state=random_state)
    U = svd.fit_transform(views)
    Sigma = np.diag(svd.singular_values_)
    Vt = svd.components_

    predictions = predict(forbidden_items, np.dot(U, Sigma), Vt,
                          num_to_user_id, num_to_id3)
    relevance = pd.DataFrame(data=predictions, columns=prediction_columns)
    relevance.to_csv(output_path, index=False)
