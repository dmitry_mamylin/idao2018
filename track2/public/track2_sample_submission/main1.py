import pandas as pd
import numpy as np

from scipy.sparse import csr_matrix
from sklearn.decomposition import TruncatedSVD

input_path = "./train.csv.zip"
output_path = "./submission.csv"

train = pd.read_csv(input_path, dtype="int32", compression="zip", engine="c")



submission = pd.DataFrame(train.user_id.unique()[:5000], columns = ['user_id'])

submission['id3_1'] = 0
submission['id3_2'] = 1
submission['id3_3'] = 2
submission['id3_4'] = 3
submission['id3_5'] = 4

submission.to_csv('submission.csv', index=False)
